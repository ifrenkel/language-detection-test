module gitlab.com/gitlab-org/security-products/tests/go-modules

go 1.12

require (
	github.com/astaxie/beego v1.10.0
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/yaml.v2 v2.2.2
)
